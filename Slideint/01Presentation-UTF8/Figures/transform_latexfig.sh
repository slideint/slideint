if test -z `echo $1 |grep .latexfig`
	then
		echo "Erreur, $1 n'est pas un fichier .latexfig"
		exit 1
fi

BASE=`echo $1 | cut -d. -f1 | cut -d/ -f2`

if ! test -f $BASE.latexfig
	then
		echo "Erreur, fichier $BASE.latexfig inexistant"
		exit 1
fi

echo '
\documentclass[a3paper]{article}
\usepackage{amsmath}          
\usepackage{amsfonts}         
\usepackage[french]{babel}    
\usepackage[latin9]{inputenc} 
%\usepackage[a0paper,margin=0cm,nohead,nofoot]{geometry} 
\usepackage{graphicx}         
\usepackage{color}            
\usepackage{epic}             
\usepackage{eepic}            
\usepackage{rotating}         
\usepackage{type1cm}          
\usepackage{epsfig}
\oddsidemargin=-0.5cm
\evensidemargin=-0.5cm
\topmargin=-1.3cm
\textheight=30cm
\textwidth=20.92cm
\headheight=1cm
\begin{document}
\thispagestyle{empty}
' > $BASE.tex

fig2dev -L pstex $BASE.latexfig > $BASE.pstex

echo -n '
\begin{picture}(0,0)
\includegraphics{'$BASE'.pstex}
\end{picture}
' >> $BASE.tex

fig2dev -L pstex_t $BASE.latexfig >> $BASE.tex
echo '\end{document}' >> $BASE.tex


latex $BASE

dvips -E -o $BASE.eps $BASE
epstopdf $BASE.eps
convert $BASE.eps $BASE.png

rm $BASE.pstex
rm $BASE.tex
rm $BASE.log
rm $BASE.aux
rm $BASE.dvi
