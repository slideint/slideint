function checkDirContent
{
  if [ $# -lt 1 ]; then
      echo checkDirContent needs one parameter
      return
  fi
  targetDir=$1
  removeName=`echo $1 | tr '[a-z]' '[A-Z]'`
  if [ ! -d $targetDir ] ; then
    cp -r ${BASEDIR}/$targetDir .
    if [ $# -eq 1 ]; then
	touch REMOVE_$removeName
    fi
  else
      for link in  ${BASEDIR}/$targetDir/* ; do
	  bfile=`basename $link`
	  if [ -f $link -a ! -e $targetDir/$bfile ]; then
	      ln -s $link $targetDir
	  fi
      done
  fi
}

function checkSubDirContent
{
  if [ $# -ne 2 ]; then
      echo checkSubDirContent needs two parameters
      return
  fi
  topDir=$1
  subDir=$2
  topName=`echo $1 | tr '[a-z]' '[A-Z]'`
  subName=`echo $2 | tr '[a-z]' '[A-Z]'`
  targetDir=$topDir/$subDir
  if [ ! -d $topDir ] ; then
      mkdir $topDir
  fi
  if [ ! -d $targetDir ]; then
    cp -r ${BASEDIR}/$targetDir $topDir
    touch REMOVE_${topName}_$subName
  else
      for link in  ${BASEDIR}/$targetDir/* ; do
	  bfile=`basename $link`
	  if [ -f $link -a ! -e $targetDir/$bfile ]; then
	      ln -s $link $targetDir
	  fi
      done
  fi
}

function unsetDirContent
{
  if [ $# -ne 1 ]; then
      echo unsetDirContent needs one parameter;
      return
  fi
  targetDir=$1
  removeName=`echo $1 | tr '[a-z]' '[A-Z]'`
  if [ -e REMOVE_$removeName ] ; then
    rm -r $targetDir 
    rm REMOVE_$removeName
  else
      for file in  $targetDir/* ; do
	  if [ -h $targetDir/$file ]; then
	      rm $targetDir/$file
	  fi
      done
  fi
}

function unsetSubDirContent
{
  if [ $# -ne 2 ]; then
      echo unsetSubDirContent needs two parameters
      return
  fi
  topDir=$1
  subDir=$2
  topName=`echo $1 | tr '[a-z]' '[A-Z]'`
  subName=`echo $2 | tr '[a-z]' '[A-Z]'`
  targetDir=$topDir/$subDir
  if [ -e REMOVE_${topName}_$subName ] ; then
    rm -r $topDir/$subDir
    rm REMOVE_${topName}_$subName
  else
      for file in  $targetDir/* ; do
	  if [ -h $targetDir/$file ]; then
	      rm $targetDir/$file
	  fi
      done
  fi
}
