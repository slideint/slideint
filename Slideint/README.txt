
Be careful! The files *.cls, *.sty and *.4ht are the same in the
different subdirectories 01Presentation-ISO, 01Presentation-UTF8,
02Course-ISO, and 02Course-UTF8. This is an assumption of the script
../debian-slideint/slideint_package.sh, and more particularly in the
commands find: e.g.
find slideint -name "*.cls" -exec mv {} texmf/tex/latex/slideint \;

Therefore, when modifying one of these files, copy and paste in the
other subdirectory.
