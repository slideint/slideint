slideint (20221102) unstable; urgency=medium

  * beamerthemes and qcmxml moved from Depends to Suggests
  * either JAVA 11 or JAVA 17

 -- Denis Conan <denis.conan@telecom-sudparis.eu>  Wed, 02 Nov 2022 14:51:12 +0100

slideint (20220428) unstable; urgency=medium

  * replace inkscape commands by cairosvg commands in make files
  * remove \RequirePackage[2020-02-02]{latexrelease}

 -- Denis Conan <denis.conan@telecom-sudparis.eu>  Thu, 28 Apr 2022 18:58:05 +0200

slideint (20220303) unstable; urgency=medium

  * move to openjdk-11-jdk and update package dependencies

 -- Denis Conan <denis.conan@telecom-sudparis.eu>  Thu, 03 Mar 2022 23:56:25 +0100

slideint (20211111) unstable; urgency=medium

  * telecom sudparis logo is larger; then, adapt scale in maketitle command

 -- Denis Conan <denis.conan@telecom-sudparis.eu>  Thu, 11 Nov 2021 09:39:00 +0100

slideint (20201231) unstable; urgency=medium

  * patch due to new version of LaTeX kernel + new options of inkscape

 -- Denis Conan <denis.conan@telecom-sudparis.eu>  Thu, 31 Dec 2020 15:59:17 +0100

slideint (20200421) unstable; urgency=medium

  * update to pdfjam: no more pdfnup, pdfjoin, etc.

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 21 Apr 2020 08:59:39 +0200

slideint (20180521) unstable; urgency=medium

  * Update to Java 9

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 21 May 2018 12:39:01 +0200

slideint (20171120) unstable; urgency=medium

  * slove conflict in options of package color/xcolor between pstricks and slideint
  * add package url to all configuration.tex

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 20 Nov 2017 14:04:44 +0100

slideint (20171117) unstable; urgency=medium

  * bug correction: conflict in options between pstricks and slideint with options of color and xcolor

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 17 Nov 2017 13:32:05 +0100

slideint (20171103) unstable; urgency=medium

  * Update to texlive (e.g. no more franchb)

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 03 Nov 2017 20:25:49 +0100

slideint (20170617-2) unstable; urgency=medium

  * no more dependency upon package pdfjam (now provided in texlive-extra-utils)

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 06 Jul 2017 12:57:27 +0200

slideint (20170617) unstable; urgency=medium

  * style files in texlive directory structure + consider empty POLY

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 06 Jul 2017 12:45:33 +0200

slideint (20160803) unstable; urgency=medium

  * Removal of the generation of PostScript files

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 03 Aug 2016 20:50:29 +0200

slideint (20160731) unstable; urgency=medium

  * bug correction: in Makefile

 -- Denis Conan <denis.conan@it-sudparis.eu>  Sun, 31 Jul 2016 22:36:11 +0200

slideint (20160728) unstable; urgency=medium

  * In 02-Course-UTF8: contenu.tex containing the command \UsesBeamer will beamer

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 28 Jul 2016 20:44:14 +0200

slideint (20160726) unstable; urgency=medium

  * In 02*, add targets other_index and other_makeindex in chapters

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 26 Jul 2016 16:49:41 +0200

slideint (20160719) unstable; urgency=medium

  * Bug correction: in 02-*, in makefile of ConfigTex4ht, first recode is for toc.pre-toc
  * Old logo removed and examples updated

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 19 Jul 2016 16:44:46 +0200

slideint (20160713) unstable; urgency=medium

  * scripts: use getopts to allow launching slideint command by writing the options -buv instead of -b -u -v

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 13 Jul 2016 11:04:33 +0200

slideint (20160712) unstable; urgency=medium

  * debian packaging: install *.cls, *.sty, and *.4ht files into /usr/local/share/texmf/tex/latex/slideint

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 12 Jul 2016 13:19:01 +0200

slideint (20160711) unstable; urgency=medium

  * Bug correction: in slideint.sty, paperwidth and paperheight are dimensions and no more macros

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 11 Jul 2016 18:23:03 +0200

slideint (20160630) unstable; urgency=medium

  * bug correction: 01, ln when dir Figures does not exist

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 30 Jun 2016 16:30:23 +0200

slideint (20150914) unstable; urgency=medium

  * bug correction: in cleaning and in public-document

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 14 Sep 2015 18:06:04 +0200

slideint (20150607) unstable; urgency=medium

  * functionality: 02-Course, remove svn export in target public-document

 -- Denis Conan <denis.conan@it-sudparis.eu>  Sun, 07 Jun 2015 18:20:49 +0200

slideint (20141008-2) unstable; urgency=medium

  * functionality: keep the one-page-per-page version for Poly (to read on screen)
  * bug correction: bug introduced in last version (target public)

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 08 Oct 2014 14:47:37 +0200

slideint (20140925) unstable; urgency=medium

  * Functionality: return back to page numbers in \label instead to slide numbers

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 25 Sep 2014 17:27:56 +0200

slideint (20140923-3) unstable; urgency=medium

  * bug correction: refactor makefile of 02Course-* for the target public

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 23 Sep 2014 17:51:59 +0200

slideint (20140923-2) unstable; urgency=medium

  * Bug correction: do not remove symbolic links in Poly/Makefile
  * Bug correction: refactor figure conversions

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 23 Sep 2014 17:19:20 +0200

slideint (20140923) unstable; urgency=medium

  * Bug correction: in the script slideint01
  * Bug correction: some variables not set when making Poly

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 23 Sep 2014 14:56:12 +0200

slideint (20140821) unstable; urgency=medium

  * Functionality: support for .latexfig ---i.e., xfig figure with LaTeX code

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 21 Aug 2014 13:58:35 +0200

slideint (20140814) unstable; urgency=medium

  * Bug correction: generation of eps and pdf figures from bitmap figures
  * Functionality: add 02Course-UTF8

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 14 Aug 2014 17:36:52 +0200

slideint (20131002) unstable; urgency=low

  * Functionality: first attempt of slideint presentations using beamer class
  * Bug correction: when cleaning, directory ConfigTeX4ht may not exist
  * Functionality: changes to prepare packaging of beamit
  * Functionality: from dvipdfm to dvipdfmx

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 02 Oct 2013 18:58:04 +0200

slideint (20120215) unstable; urgency=low

  * Bug correction: logo file name in exemples 01Presentation

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 15 Feb 2012 21:12:45 +0100

slideint (20111124) unstable; urgency=low

  * Bug correction: remove option dvipdfm from package graphicx in slideint.sty

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 24 Nov 2011 14:32:01 +0100

slideint (20111119) unstable; urgency=low

  * Bug correction: move dependency to graphicx to slideint.sty

 -- Denis Conan <denis.conan@it-sudparis.eu>  Sat, 19 Nov 2011 19:54:43 +0100

slideint (20111109) unstable; urgency=low

  * Fix dependencies: no more sun-java6-jre in debian testing

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 09 Nov 2011 18:23:46 +0100

slideint (20111023) unstable; urgency=low

  * Refactoring: refactoring of the shell scripts

 -- Denis Conan <denis.conan@it-sudparis.eu>  Sun, 23 Oct 2011 20:06:05 +0200

slideint (20111006-2) unstable; urgency=low

  * Bug correction: correct bug introduced in 02Course/Poly/Makefile

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 06 Oct 2011 15:09:42 +0200

slideint (20111006) unstable; urgency=low

  * Bug correction: rpm not deployed in previous deployment

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 06 Oct 2011 11:16:29 +0200

slideint (20111005-2) unstable; urgency=low

  * Bug correction: minor to correct previous packaging

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 05 Oct 2011 16:55:04 +0200

slideint (20111005) unstable; urgency=low

  * Functionality: insert a PDF file as a pseudo-course

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 05 Oct 2011 15:58:52 +0200

slideint (20110927) unstable; urgency=low

  * Bug correction: remove symbolic links to old logos

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 27 Sep 2011 15:59:15 +0200

slideint (20110920) unstable; urgency=low

  * Bug correction: pb in deployment site

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 20 Sep 2011 16:41:35 +0200

slideint (20110902-2) unstable; urgency=low

  * Bug correction: remove old logo of TMSP

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 02 Sep 2011 08:32:47 +0200

slideint (20110902) unstable; urgency=low

  * Bug correction: forgotten for the makefile of the directory O2Course/Poly

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 02 Sep 2011 08:23:42 +0200

slideint (20110901-2) unstable; urgency=low

  * Bug correction: same bug correction for 02Course

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 01 Sep 2011 20:55:57 +0200

slideint (20110901) unstable; urgency=low

  * Bug correction: insert a latex compilation before makeindex so that documents with table of contents and bibliography of several pages have the right page numbers in their indexes

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 01 Sep 2011 19:25:33 +0200

slideint (20110809) unstable; urgency=low

  * Functionality: document last option -u for UTF-8 character encoding.

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 09 Aug 2011 08:17:31 +0200

slideint (20110808) unstable; urgency=low

  * Functionality: 01Presentation in ISO mode versus in UTF8

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 08 Aug 2011 18:36:14 +0200

slideint (20110805) unstable; urgency=low

  * Bug correction: add argument -cp with classpath complemented with current dir for execution of java command


 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 05 Aug 2011 16:26:22 +0200

slideint (20110713-2) unstable; urgency=low

  * Refactoring: of makefile of 01Presentation

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 13 Jul 2011 17:07:51 +0200

slideint (20110713-1) unstable; urgency=low

  * Bug correction: add debian package dependency to pdfjam for the command pdfnup, remove package dependency gv

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 13 Jul 2011 16:35:24 +0200

slideint (20110713) unstable; urgency=low

  * Refactoring: of makefile of 01Presentation

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 13 Jul 2011 16:20:12 +0200

slideint (20110624) unstable; urgency=low

  * Functionality: new target post-public-document in 02Course

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 24 Jun 2011 13:48:20 +0200

slideint (20110507-1) unstable; urgency=low

  * Functionality: review dependencies and add OTHER_DEPENDENCIES

 -- Denis Conan <denis.conan@it-sudparis.eu>  Sat, 07 May 2011 22:26:40 +0200

slideint (20110507) unstable; urgency=low

  * Functionality: remove Makefile-figure and replace by make dependencies

 -- Denis Conan <denis.conan@it-sudparis.eu>  Sat, 07 May 2011 17:21:48 +0200

slideint (20110225) unstable; urgency=low

  * Bug correction: building of symbolic links for ConfigTex4ht/fill.txt and ConfigTex4ht/end_file.txt

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 25 Feb 2011 12:42:39 +0100

slideint (20110223) unstable; urgency=low

  * Bug correction: add comment on target cleanpublic in the man page.

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 23 Feb 2011 21:35:34 +0100

slideint (20110124) unstable; urgency=low

  * Bug correction: translation of next links in English and in French

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 24 Jan 2011 21:39:35 +0100

slideint (20101104) unstable; urgency=low

  * Bug correction: snapshot version erroneously deployed

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 04 Nov 2010 21:39:38 +0100

slideint (20101102) unstable; urgency=low

  * Bug correction: some terms translated in English when required

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 02 Nov 2010 15:34:14 +0100

slideint (20101019) unstable; urgency=low

  * Bug correction: pb in previous deployment

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 19 Oct 2010 09:53:43 +0200

slideint (20101018) unstable; urgency=low

  * Bug correction: in Makefile-configure, cf. last deloyment

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 18 Oct 2010 12:46:04 +0200

slideint (20101014) unstable; urgency=low

  * Functionality: possibility to use macros of Makefile-configure with argument LIST of the slideint command, e.g. slideint chap LIST=Cours

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 14 Oct 2010 17:14:14 +0200

slideint (20100916) unstable; urgency=low

  * Functionality: tutorials in /usr/share/doc/slideint and URL of main mailing list in home page

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 16 Sep 2010 22:07:45 +0200

slideint (20100907) unstable; urgency=low

  * Bug correction: print nothing when theotherauthor is empty

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 07 Sep 2010 11:21:47 +0200

slideint (20100824) unstable; urgency=low

  * Bug correction: language in man page

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 24 Aug 2010 09:07:32 +0200

slideint (20100817) unstable; urgency=low

  * Functionality: new option -v can replace option VERBOSE=yes

 -- Denis Conan <denis.conan@it-sudparis.eu>  Tue, 17 Aug 2010 18:44:06 +0200

slideint (20100729) unstable; urgency=low

  * Functionality: new environment slidecontentinimage to insert local content in an image in the HTML version (generated with TeX4ht)

 -- Denis Conan <denis.conan@it-sudparis.eu>  Thu, 29 Jul 2010 13:11:06 +0200

slideint (20100728) unstable; urgency=low

  * Bug correction: add dependency to package texlive-lang-all

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 28 Jul 2010 12:29:02 +0200

slideint (20100519) unstable; urgency=low

  * Functionality: in 02Course/Poly, PDF Level 1.5 with all the fonts embedded

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 19 May 2010 19:44:13 +0200

slideint (20100512) unstable; urgency=low

  * Functionality: new icon simulation.png|eps

 -- Denis Conan <denis.conan@it-sudparis.eu>  Wed, 12 May 2010 16:23:01 +0200

slideint (20100322) unstable; urgency=low

  * Bug correction: link forgotten

 -- Denis Conan <denis.conan@it-sudparis.eu>  Mon, 22 Mar 2010 10:14:36 +0100

slideint (20100206) unstable; urgency=low

  * Functionality: new mode documented in man page

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 05 Feb 2010 18:38:06 +0100

slideint (20100205) unstable; urgency=low

  * Functionality: four pages per page mode

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 05 Feb 2010 18:29:41 +0100

slideint (20100116) unstable; urgency=low

  * Packaging: signature (complement)

 -- Denis Conan <denis.conan@it-sudparis.eu>  Fri, 15 Jan 2010 16:48:43 +0100

slideint (20100115) unstable; urgency=low

  * Packaging: signature

 -- Denis <denis.conan@it-sudparis.eu>  Fri, 15 Jan 2010 14:25:06 +0100

slideint (20100106) unstable; urgency=low

  * Packaging: replace dviutils with texlive-extra-utils

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Wed, 06 Jan 2010 08:43:35 +0100

slideint (20091127) unstable; urgency=low

  * Functionality: support for figures in files .dia and .svg with software dia and inkscape.

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Fri, 27 Nov 2009 15:55:44 +0100

slideint (20091104) unstable; urgency=low

  * Functionality: new command \slidenumversion for title page

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Wed, 04 Nov 2009 14:13:40 +0100

slideint (20091013) unstable; urgency=low

  * Bug correction: comma after author and before authorother in HTML generation

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Tue, 13 Oct 2009 15:14:18 +0200

slideint (20090908) unstable; urgency=low

  * Functionality: second list of authors

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Tue, 08 Sep 2009 09:14:08 +0200

slideint (20090903) unstable; urgency=low

  * Functionality: in O2Course, new target public-document and optio force added to svn export

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Thu, 03 Sep 2009 13:31:51 +0200

slideint (20090728) unstable; urgency=low

  * Functionality: remove target post-dvi in Makefile and Makefile-configure

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Tue, 28 Jul 2009 13:55:17 +0200

slideint (20090622) unstable; urgency=low

  * Problem when deploying the RPM package

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Mon, 22 Jun 2009 16:33:05 +0200

slideint (20090612) unstable; urgency=low

  * Functionality: replace temporary files .txt by .delme so that they do not collide with end-user README.txt files (thanks to Daniel for having pointed this

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Fri, 12 Jun 2009 18:11:24 +0200

slideint (20090611-1) unstable; urgency=low

  * Bug in debian packaging

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Thu, 11 Jun 2009 11:19:32 +0200

slideint (20090611) unstable; urgency=low

  * Functionality: debian packaging for all the architectures and small fixes in man page

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Thu, 11 Jun 2009 10:51:08 +0200

slideint (20090608) unstable; urgency=low

  * Documentation: man page improved

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Mon, 08 Jun 2009 07:40:21 +0200

slideint (20090602) unstable; urgency=low

  * Bug correction: renaming forgotten in script slideint02

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Tue, 02 Jun 2009 17:30:10 +0200

slideint (20090526) unstable; urgency=low

  * Functionalities: add target tout in 01Presentation and replace target all by tout, adapt documentations and target default in files Makefile-Configure

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Tue, 26 May 2009 10:27:30 +0200

slideint (20090518) unstable; urgency=low

  * Functionalities (man page) + refactoring (align 01Presentation and 02Course)

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Mon, 18 May 2009 11:05:29 +0200

slideint (20090511) unstable; urgency=low

  * Bug correction in 02Course/poly: error of +4 pages in page number of index

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Mon, 11 May 2009 09:44:22 +0200

slideint (20090304) unstable; urgency=low

  * Bug correction: headers when no sectioning

 -- Denis Conan <denis@grenat.it-sudparis.eu>  Wed, 04 Mar 2009 14:15:44 +0100

slideint (20081120-2) unstable; urgency=low

  * redeploy for rpm pb

 -- Denis Conan <denis@pelee.int-evry.fr>  Thu, 20 Nov 2008 17:28:28 +0100

slideint (20081120-1) unstable; urgency=low

  * Deactivation of command \tableofcontents when generating HTML pages

 -- Denis Conan <denis@pelee.int-evry.fr>  Thu, 20 Nov 2008 16:49:22 +0100

slideint (20081120) unstable; urgency=low

  * Bug correction: when not a slide presentation, no toc*, then protect
    rm commands

 -- Denis Conan <denis@pelee.int-evry.fr>  Thu, 20 Nov 2008 16:39:10 +0100

slideint (20081117) unstable; urgency=low

  * Functionality: in class newslide, add the storing of formating rules of
    section, subsections and subsections, and the command
    \renewsectionoriginformat to renew the original formating rules

 -- Denis Conan <denis@pelee.int-evry.fr>  Mon, 17 Nov 2008 09:04:59 +0100

slideint (20081016) unstable; urgency=low

  * Bug correction: space between items set the minimum, like in French

 -- Denis Conan <denis@pelee.int-evry.fr>  Thu, 16 Oct 2008 19:50:59 +0200

slideint (20080930) unstable; urgency=low

  * Bug correction: problem in creating rpm packages

 -- Denis Conan <denis@pelee.int-evry.fr>  Tue, 30 Sep 2008 17:14:32 +0200

slideint (20080923-1) unstable; urgency=low

  * Bug correction: typo

 -- Denis Conan <denis@pelee.int-evry.fr>  Tue, 23 Sep 2008 18:45:59 +0200

slideint (20080923) unstable; urgency=low

  * Bug correction: target post-public renamed post-public-global
    in file ./Makefile-configure

 -- Denis Conan <denis@pelee.int-evry.fr>  Tue, 23 Sep 2008 17:38:03 +0200

slideint (20080905) unstable; urgency=low

  * Bug correction: in target html, call recode after the cp
    + rename corresponding directories

 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 05 Sep 2008 17:17:25 +0200

slideint (20080903) unstable; urgency=low

  * Bugs correction:
    - target chapens of Poly
    - recode of BASEHTML*.html pages
    - when copying files of $(RESSOURCESDIR)/Images to Public/EnLigne/Images
    add option "-H" to "cp -r" so that symbolic links are not copied as
    symbolic links but "follow command-line symbolic links in SOURCE"

  * Addition of target post-public to add some treatments after svn exporting
    of directory Documents, e.g. recode some HTML files
    - this new target MUST be added in all the files ./Makefile-configure
      - THUS every project MUST adapt to be conformant to this new rule

 -- Denis Conan <denis@pelee.int-evry.fr>  Wed, 03 Sep 2008 08:32:37 +0200

slideint (20080818) unstable; urgency=low

  * Bug correction: cp of ${BASEHTML}*.html pages into EnLigne/Contenu

 -- Denis Conan <denis@pelee.int-evry.fr>  Mon, 18 Aug 2008 15:10:52 +0200

slideint (20080725-2) unstable; urgency=low

  * simplify the process for generating HTML pages
    - remove echo tidy commandes
    - remove file ${BASEHTML} in the result directories
    - rename file Accueil.html to index.html
      and remove the creation of the link from index.html to Acceuil.html

 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 25 Jul 2008 16:55:34 +0200

slideint (20080725-1) unstable; urgency=low

  * Bug correction: call of other_* in target chapens of O2Course/Poly

 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 25 Jul 2008 11:51:23 +0200

slideint (20080725) unstable; urgency=low

  * Bug correction: logo on HTML page larger

 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 25 Jul 2008 11:14:37 +0200

slideint (20080724) unstable; urgency=low

  * New functionality: In target %.dvi of 01Presentation
    and in targets chap, chapens, html of 02Course
    and in targets of 02Course/Poly
    - insertion of calls to two new targets called other_bibtex other_makeindex
      - as their names suggest, to allow specific calls to bibtex and makeindex
    - these two new targets should be defined in the corresponding files
      Makefile-configure
      - see the reference directories for examples
    - the calls to these targets is of the form make other_target NEW_MACRO=$*
      - the new macros are BIBTEXFILE and MAKEINDEXFILE, respectively
      - then, you can use them in Makefile-configure to know the files
      	currently under compilation


 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 25 Jul 2008 10:26:29 +0200

slideint (20080711) unstable; urgency=low

  * Bug correction: No more need to change locale
    The script slideint does this for the user

 -- Denis Conan <denis@pelee.int-evry.fr>  Sat, 12 Jul 2008 08:51:51 -0400

slideint (20080708) unstable; urgency=low

  * Bug correction:
    - when no mainlogo and no propriologo ---i.e., \empty
      do not include any graphic and do not insert any text
  * Target clean: remove *.fig.bak files


 -- Denis Conan <denis@pelee.int-evry.fr>  Tue, 08 Jul 2008 08:47:45 +0200

slideint (20080707) unstable; urgency=low

  * Bug correction: When no logo, put the propriotext on the first page

 -- Denis Conan <denis@pelee.int-evry.fr>  Mon, 07 Jul 2008 07:37:45 +0200

slideint (20080402) unstable; urgency=low

  * New functionality
    - up to now, only global bib in polys OR must modify Poly/Makefile to
      add calls to bibtex in targets chap and chapens
    - now, local + global bib without a local copy of Poly/Makefile
    - addition of targets autresbibtexchap and autresbibtexchapens
      in Poly/Makefile-configure
      - e.g., bibtex course1-notes for a local bib to Cours/Course1 in poly.pdf
      - e.g., bibtex course1-notes-ens for a local bib to Cours/Course1
      	in poly-ens.pdf
    - call to make autresbibtexchap in target chap of Poly/Makefile
    - call to make autresbibtexchapens in target chapens of Poly/Makefile

 -- Denis Conan <denis@pelee.int-evry.fr>  Thu, 03 Apr 2008 08:46:58 +0200

slideint (20080331) unstable; urgency=low

  * Bug correction: index not generated for poly.pdf when no slideprintindex
    command in courses, but one in fin.tex
    - add $(wildcard *.tex) to SOURCES in Poly

 -- Denis Conan <denis@pelee.int-evry.fr>  Mon, 31 Mar 2008 10:29:40 +0200

slideint (20080330) unstable; urgency=low

  * Bug correction: scripts slideint01 and slideint02 updated to create
    links and removing them for the new logos


 -- Denis Conan <denis@pelee.int-evry.fr>  Sun, 30 Mar 2008 20:30:12 +0200

slideint (20080326) unstable; urgency=low

  * Bug correction: O2Course
    svn export not in Ressources/Makefile but in ./Makefile
    + new variable DOCDIR in Makefile-configure to provide the name of the
      directory to export in Public without any treatments

 -- Denis Conan <denis@pelee.int-evry.fr>  Thu, 27 Mar 2008 09:30:26 +0100

slideint (20080317) unstable; urgency=low

  * new logos everywhere and at the appropriate sizes
  * add variable SHELL=/bin/bash in Makefiles

 -- Denis Conan <denis@pelee.int-evry.fr>  Mon, 17 Mar 2008 17:30:08 +0100

slideint (20080306) unstable; urgency=low

  * Removing of all the generation process of files *.stoc required by
    moodle. From now, this should to be entirely done in target post-public.
  * Bug correction: Introduce a space between pre-chapens and figures in
    target chapens of file 02Course/Ressources/Makefile.

 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 07 Mar 2008 07:42:53 +0100

slideint (20080304) unstable; urgency=low

  * New logos for 01Presentation

 -- Denis Conan <denis@pelee.int-evry.fr>  Tue, 04 Mar 2008 13:16:20 +0100

slideint (20080222) unstable; urgency=low

  * add english to babel option in configuration.tex
    * by default, \selectlanguage{frenchb}
    * users may add \selectlanguage{english} to change to English speaking

 -- Denis Conan <denis@pelee.int-evry.fr>  Sat, 23 Feb 2008 17:47:17 +0100

slideint (20080218) unstable; urgency=low

  * Changes in newslide:
    * Removal of the style semcolor, to be replaced in slideint by the style
      color
    * Comment added to the last section explaining why there is no layers in
      PDF files, due to dvipdfm
  * Changes in slideint:
    * Bug correction: coloring is ok now for HTML pages, style color added with
      the option usenames so that all the dvips system colors can be used

 -- Denis Conan <denis@pelee.int-evry.fr>  Mon, 18 Feb 2008 08:46:44 +0100

slideint (20080205) unstable; urgency=low

  * debian packaging: sun-java5-jdk replaced by sun-java5-jre | sun-java6-jre

 -- Denis Conan <denis@pelee.int-evry.fr>  Tue, 05 Feb 2008 15:39:42 +0100

slideint (20080125) unstable; urgency=low

  * correction bug link "suivant" missing in HTML pages:
    add a latex compilation when no bibbliography and no index
  * correction bug target liens-ressources: in dependencies, this target must
    be called before the target pre-* because the execution of the latter
    may need results of the execution of the former
  * null spacing between items with command \noitemspace: command called
    by default in slideint.sty
  * command \license to be inserted by users in their presentations
  * addition of tests in Makefile-figure before calling shell command convert
    so that output files are not overriden if they already exist
  * factorisation of index style files
  * bug correction : handling ISO character in shell makeindextex4ht.sh
  * addition of command \slideindex to replace command \index


 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 25 Jan 2008 07:34:59 +0100

slideint (20080111) unstable; urgency=low

  * Remove rights x on regular files
  * Add directories Figures + examples of \includegraphics
  * Replace dependency texlive-full by texlive + texlive-pstricks
  * Add dependency dviutils for dviselect
  * Rename /usr/share/slideint/examples into /usr/share/slideint/Examples

 -- Denis Conan <denis@pelee.int-evry.fr>  Fri, 11 Jan 2008 11:48:17 +0100

slideint (20080109) unstable; urgency=low

  * Add dependency to sun-java5-jdk
  * Replace dependency texlive by texlive-full (to include pstricks.sty)

 -- Denis Conan <denis@pelee.int-evry.fr>  Wed, 09 Jan 2008 18:31:25 +0100

slideint (20080103) unstable; urgency=low

  * quelques paquetages LaTeX supplementaires dans les fichiers configuration.tex
  * Bonne Année 2008 !

 -- Denis Conan <denis@pelee.int-evry.fr>  Thu, 03 Jan 2008 11:11:21 +0100

slideint (20071231) unstable; urgency=low

  * Example directory in /usr/share/slideint/examples, /usr/share/doc/slideint/examples to be a link to the previous directory
  * Correction bug: add redirect in 01Presentation/Makefile

 -- Denis Conan <denis@pelee.int-evry.fr>  Mon, 31 Dec 2007 08:02:36 +0100

slideint (20071229) unstable; urgency=low

  * New script to update and deploy the package slideint
  * Correction package bug: gunzip the files of /usr/share/doc/slideint/examples/
  * Correction slideint bug: in 02Course/Poly/Makefile, in targets chap and chapens, call liens before pre-chap/pre-chapens

 -- Denis Conan <denis@pelee.int-evry.fr>  Sat, 29 Dec 2007 15:25:14 +0100

slideint (20071220) unstable; urgency=low

  * Initial Release.

 -- Denis Conan <denis.conan@int-edu.eu>  Thu, 20 Dec 2007 14:53:26 +0100
