#!/bin/bash

# memorise the current working directory
oldrep=${PWD}
echo Give the version name of the package \(e.g. YearMonthDay\)
read x
# in the directory debian of slideint
cd ${HOMESLIDEINT}/Packaging-Debian/
# launch the script that creates the package
./slideint_package.sh ${x} ${HOMESLIDEINT}/APT_Debian_repository
cp ${HOMESLIDEINT}/APT_Debian_repository/binary/slideint_${x}_all.deb /tmp
# in directory /tmp
cd /tmp
cp slideint*.changes ${HOMESLIDEINT}/APT_Debian_repository/sources/
cp slideint*.dsc ${HOMESLIDEINT}/APT_Debian_repository/binary/
find ${HOMESLIDEINT}/APT_Debian_repository/ -type d -exec chmod a+rx {} \;
find ${HOMESLIDEINT}/APT_Debian_repository/ -type f -exec chmod a+r {} \;
echo Update and deployment done.
ls -lR ${HOMESLIDEINT}/APT_Debian_repository/
mv ${HOMESLIDEINT}/Packaging-Debian/changelog ${HOMESLIDEINT}/Packaging-Debian/changelog.old
mv /tmp/changelog ${HOMESLIDEINT}/Packaging-Debian/
echo END
cd ${oldrep}
