# LaTeX2e slideint style and ecosystem



This project proposes a LaTeX class (`newslide.cls`) for
structuring LaTeX slides and a LaTeX style (`slideint.sty`)
for organising the slides' layout according to our institution
(Institut Mines-T&eacute;l&eacute;com, T&eacute;l&eacute;com
SudParis). The class can be used without the style.

Slides are of two "types": seminar slides and beamer slides. The usage of beamer has been introduced in 2016.

For "installing" `slideint`, please refer to the section
"Installation" below that gives the information for getting the
GNU/Linux `debian` package.

## Getting started

Before running the command `slideint`, try `man
slideint`. Then, make sure your are in a repository containing a
file `contenu.tex` and your `locale` are set to a value
stipulating the character set `ISO-8859-15`
or `UTF-8`. Next, run `slideint` with no arguments to
see the different usages of the command.

The `slideint` LaTeX style depends upon the `newslide`
LaTeX class. The style `slideint` contains configurations of
the layout of seminar slides sectioned with the
class [`newslide`](./Docs/newslide/index.html) for Institut Polytechnique de Paris, Télécom SudParis. Of
course, you can change the logo for your institution's one. To know
more about this class, recall that the source code file of the class
contains its proper documentation.

Since July 2016, in `O2-Course-UTF8`, presentations can
use *beamer*: this is detected by inserting the
command `\UsesBeamer` in the file `contenu.tex`. The
theme that is used is `tsp` defined in
package [`beamerthemes`](https://gitlab.inf.telecom-sudparis.eu/slideint/beamerthemes). Of course, you can use the beamer theme without slideint: See the [`beamerthemes`](https://gitlab.inf.telecom-sudparis.eu/slideint/beamerthemes) project.

## Documentation

Man page of `slideint` utility:
* [HTML](./Docs/man-slideint/slideint.html)
* [PDF](./Docs/man-slideint/slideint.pdf)

## Examples and reference guide for seminar slides

Tutorial guide `*-01Presentation`:
* [Slides for presentation](./Docs/tutorials-slideint/01Presentation/diapos.pdf)
* [Slides, 2 per page](./Docs/tutorials-slideint/01Presentation/diapos_2pp.pdf)
* [Slides with notes](./Docs/tutorials-slideint/01Presentation/chap.pdf)
* [Source code](./Docs/tutorials-slideint/01Presentation/contenu.tex)

Reference guide:
* [Slides for presentation](./Docs/reference/diapos.pdf)
* [Slides, 2 per page](./Docs/reference/diapos_2pp.pdf)
* [Slides, format `article`](./Docs/reference/chap.pdf)


## Installations

A scenario with commands is provided at the end of this section.

Debian packages are available on directory  [APT_Debian_repository](./APT_Debian_repository/). For instance, Debian package file [APT_Debian_repository/binary/slideint_20220428_all.deb](APT_Debian_repository/binary/slideint_20220428_all.deb).

Check also that the following *locales* have been installed:
* `fr_FR@euro` for `ISO-8859-15`
* `fr_FR` for `ISO-8859-15`

The dependencies of the Debian package are listed in file [./Packaging-Debian/control](./Packaging-Debian/control).

You can also use the Docker image: See GitLab projet [slideint-docker](https://gitlab.inf.telecom-sudparis.eu/slideint/slideint-docker)

The installation can be checked using the following commands:
```
$ kpsewhich slideint.sty
/usr/local/share/texmf/tex/latex/slideint/slideint.sty
$ kpsewhich slideint.4ht
/usr/local/share/texmf/tex/latex/slideint/slideint.4ht
$ kpsewhich newslide.cls
/usr/local/share/texmf/tex/latex/slideint/newslide.cls
$ kpsewhich newslide.4ht
/usr/local/share/texmf/tex/latex/slideint/newslide.4ht
$ man slideint
```


Practically, execute the following commands:
```
$ locale -a
...
fr_FR
fr_FR@euro
fr_FR.iso88591
fr_FR.iso885915@euro
fr_FR.utf8
...
Either manually or using wget, download the deb file https://gitlab.inf.telecom-sudparis.eu/slideint/slideint/-/blob/main/APT_Debian_repository/binary/slideint_20220428_all.deb
...
$ sudo apt-get install -f ./slideint_20220428_all.deb 
...
$ kpsewhich slideint.sty
/usr/local/share/texmf/tex/latex/slideint/slideint.sty
$ kpsewhich slideint.4ht
/usr/local/share/texmf/tex/latex/slideint/slideint.4ht
$ kpsewhich newslide.cls
/usr/local/share/texmf/tex/latex/slideint/newslide.cls
$ kpsewhich newslide.4ht
/usr/local/share/texmf/tex/latex/slideint/newslide.4ht
$ man slideint
...
```

## Known bugs

* In `02Courses-*`, pseudo chapters have been removed from the build path because of a bug with `\includepdf` (an interaction with `combine`?).


## Changes

* 2nd November 2022: in Debian packaging, `openjdk-17-jdk |
  openjdk-11-jdk`, and `qcmxml` and `beamerthemes` in `Suggests` only.


* 28th April 2022: replace `inkscape` commands by `cairosvg` commands in make files, and remove `\RequirePackage[2020-02-02]{latexrelease}`

* 4th March 2022: Move to OpenJKD-11 and update Debian package dependencies.

* 11th November 2021: Télécom SudParis logo is larger; then, adapt scale in maketitle command

* 31st Decembre 2020: patch of the version of LaTeX due to the new version of LaTeX kernel:
    * A general hook management system for LaTeX
    * [https://www.latex-project.org/news/2020/07/22/latex-dev-2020-10-1b/](https://www.latex-project.org/news/2020/07/22/latex-dev-2020-10-1b/)
    * [https://www.mail-archive.com/lyx-devel@lists.lyx.org/msg213669.html](https://www.mail-archive.com/lyx-devel@lists.lyx.org/msg213669.html)

* 04th April 2020: update to `pdfjam` (no more `pdfnup`, `pdfjoin`, etc.)

* 21st May 2018: update to `JAVA 9` (deprecated `Boolean` constructors).

* 20th November 2017: add package `url` to all the files `configuration.tex` (seminar slides).

* 17th November 2017: correction of conflict in options of package   `color`/`xcolor` between `pstricks` and `slideint`.

* 06th July 2017: style files in `texlive` directory and consider empty `POLY`; debian package no more dependent upon the package `pdfjam` (now virtual and included in package `texlive-extra-utils` since last debian stable release).

* 03rd August 2016: Removal of the generation of `PostScript` files.

* 28th July 2016: In `02-Course-UTF8`, `contenu.tex` containing the command `\UsesBeamer` will `beamer`.

* 26th July 2016: in `02Course*`, add targets `other_bibtex` and `other_makeindex` in `Makefile-configure` of chapters in order to add other bibtex files and other indexes or glossaries.

* 19th July 2016: bug correction in `Makefile` of `ConfigTex4ht` on first `recode` (file name); remove old logo and update.

* 13th July 2016: in scripts, use `getopts` to allow launching `slideint` command by writing the options `-vub` instead of `-b -u -v`; `man` page updated.

* 12th July 2016: debian packaging: install `*.cls`, `*.sty`, and `*.4ht` files into `/usr/local/share/texmf/tex/latex/slideint` so that `LaTeX` commands can find them (try the `kpsewhich` command on these files to check that `LaTeX` can find them; see the section on the installation).
    * The style `slideint.sty` no more contains the instruction `\RequirePackage[xxx]{inputenc}`, where `xxx` is either `latin1` or `utf8`. Therefore, users have to adapt the files `configuration.tex` of their existing projects to add that instruction.

* 11th July 2016: bug correction: `paperwidth` and `paperheight` are dimensions and no more macros.

* 05th July 2016: debian repository and instructions are recovered.

* 30th June 2016: reorganise debian packaging and deploy Web pages on `fusionforge.int-evry.fr`. Also, correct bugs in character encoding of HTML pages for `UTF-8`.

* 5th April 2016: move all the project from `picoforge.int-evry.fr` to `fusionforge.int-evry.fr`.

* 01st January 2016: in `slideint01`, bug correction when creating a link when directory `Figures` does not exist.

* 14th September 2015: in `02Course*`, bug correction in target public-document` and in cleaning.

* 7th June 2015: in `02Course*`, removal of the `svn export` in target `public-document` in order to support not only `Subversion` repositories but other repositories such as `Git`.

* 8th October 2014: keep the one-page-per-page version for Poly (to be
read on screen).

* 25th September 2014: add `francais` to `babel` options, and in `newslide.cls`, remove redefinition of the command `\label` to return back to page numbers instead of slide number.

* 23rd September 2014: variable `BIBTEXFILE` and `MAKEINDEXFILE` not set in `Poly/Makefile`, do not remove symbolic links in `Poly/Makefile`, refactor figure conversions, and refactor `makefile` of `02Course-*` for the target `public`.

* 21st August 2014: add support of `Xfig` figures with LaTeX code fragments.

* 14th August 2014: add `02Course-UTF8`, `02Course` renamed `02Course-ISO`.

* 05th April 2014: do not generate `eps`>, `pdf`, and `png` figures from `gif`, `png`, and `jpg` figures.

* 30th April 2014: correct links in this Web page.

* 02nd October 2013: solve package deployment (`dch` command with `emacs`
mode) and move from `dvipdfm` to `dvipdfmx`.

* 14th May 2013: preparation of the arrival of `beamit`.

* 29th May 2012: slides with beamer; new `slideint` command option `--beamer`; new source code directory `PresentationSlideBeamer-UTF8`.

* 26th May 2012: add translation of `Xfig` figures that include LaTeX source code.

* 15th February 2012: Correct bug in logo file name in examples `01Presentation-*`.

* 24th November 2011: Remove option `dvipdfm` of `graphicx` to let projects choose a particular option: `dvipdfm`, `pdftex`, etc.

* 17th November 2011: Add `theotherauthor` and `theversionnumber` in `02Course`, `Poly`.

* 09 November 2011: Fix debian dependencies because `sun-java6-jre` has been removed from `debian testing`.

* 23 October 2011: Refactoring of the shell scripts to extract and factorize functions.

* 05 October 2011: new functionality in `02Course` to insert a PDF file as a pseudo-course.

* 27 September 2011: remove symbolic links to old logos in th scripts.

* 02 September 2011: remove old logos for TMSP.

* 01 September 2011: insert a latex compilation before makeindex so that documents with table of contents and bibliographies of several pages have the right page numbers in their indexes.

* 09 August 2011: in `O1Presentation*`, add possibility to encode source files using UTF-8 characters. Therefore, source code can be encoded in either ISO-8859-15 characters or UTF-8 characters. Use the option `-u` of the command `slideint` for UTF-8 characters.

* 05 August 2011: add to `java` commands the argument `-cp` with `CLASSPATH complemented with current directory.

* 13 July 2011: complete refactoring of the `Makfile` of `01Presentation`. Add debian package dependency to `pdfjam` for the command `pdfnup`. No more support for the generation of PostScript files.

* 07 May 2011: add listings package in `configuration.tex`.

* 25 February 2011: bug correction in the scripts `slideint01` and `slideint02` (symbolic links of files in `ConfigTex4ht`).

* 23 February 2011: add comment on target `cleanpublic` in the man page.

* 24 January 2011: bug correction in the translation of the links `next`.

* 02 November 2010: some terms in HTM pages translated in English when required.

* 18 October 2010: new possibility for the command `slideint` for `02Course`: use macros defined in `Makefile-configure`, such as `slideint chap LIST=Cours`

* 17 September 2010: examples and tutorials in `/usr/shared/doc/slideint/examples`, and URL of main mailing list archives in this home page.

* 7 Septembre 2010: correction of the signature at the bottom of HTML pages.

* 24 August 2010: correction of man pages.

* 17 August 2010: new option `-v`> for verbose mode, instead of `VERBOSE=yes`.

* 29 July 2010: new environment `slidecontentinimage` to insert local content in an image in HTML version generated with TeX4ht.

* 28 July 2010: notes in this web page for Ubuntu users concerning installation and addition of dependency to package `texlive-lang-all`.

* 19 May 2010: in `02Course/Poly`, PDF generated with level 1.5 and all the fonts embedded.

* 05 February 2010: add new command for generating the slides (only the slides) in the mode "four pages per page".

* 27 November 2009: support for figures in files `.dia` and `.svg` with software `dia` and `inkscape`.

* 04 November 2009: new command `\slidenumversion` to add for instance subversion revision number in the title page.

* 13 October 2009: remove comma after author when authorother is empty.

* 08 September 2009: add a second line of authors that can be assigned with the command `slideauthorother`. By default, this second list of authors is empty.

* 03 September 2009: add target `public-document` to publish the `DOCDIR` without calling target `public` on all directories, and option `--force` to force the export even if the `DOCDIR` has already been exported by a call to `make public`.

* 02nd June 2009: bug correction (file renaming forgotten in script `slideint02`).

* 26th May 2009: add target tout in `01Presentation` and replace target all by tout, adapt documentations and target default in files `Makefile-Configure`.

* 18th May 2009: 
    * In 01Presentation,
        * Align targets of `01Presentation` to `02Course`'s ones: additions of targets `chap`, `chapens`, and `diapos`.
        * Refactoring of target default.
        * Output directory `Enligne` renamed `Web`.
        * Simplification of the content of output directory Web: no more sub-directories `Contenu` and `Documents`; no more tgz archive creation.
    * In 02Cours,
        * Refactoring of target `default`.
        * Output directory `Enligne` renamed `Web`.
        * Add directory `example_slideint`.
        * `02Course/Makefile-configure` updating to changes in `02Course`
        * Bug correction: `02Course` example needs a directory `Poly` with files `debut*.tex`, `fin.tex` and `bib.bib`.
    * In Web:
        * Refactoring of index page in sections.
        * Addition of a man page for the `slideint` utility.
        * Updating of the tutorial of `01Presentation`.
        * Updating of the reference presentation.

* 13th May 2009: file and target renaming:
    * In `01Presentation`, `transparents` renamed `diapos`.
    * In `01Presentation`, `article_enseignants` renamed `chapens`.
    * In `01Presentation`, `article` renamed `chap`.
    * In `01Presentation`, `transparentsHtml` renamed `html`.
    * In `O2Course`, `slides` renamed `diapos`.
    * In `O2Course`, `notes` renamed `chap`.
    * In `O2Course`, `notes-ens` renamed `chapens`.

* 11th May 2009: in `02Course/poly`, correction of the bug that erroneously adds 4 pages to index pages.

* 15th November 2008: in class `newslide`, add the storing of formating rules of section, subsections and subsections, and the command `\renewsectionoriginformat` to renew the original formating rules.

* 3rd September 2008:
    * Bugs correction:
        * Target chapens of `Poly`.
        * Recode of `BASEHTML*.html` pages.
        * when copying files of `$(RESSOURCESDIR)/Images` to `Public/EnLigne/Images`, add option `-H` to `cp -r` so that symbolic links are not copied as symbolic links but *follow command-line symbolic links in source*.
    * Addition of target `post-public` to add some treatments after `svn export`ing directory `Documents`, e.g. `recode` some `HTML` files. This new target must be added in all the files `./Makefile-configure`. Thus, every project must adapt to be conformant to this new rule.

* 25 July 2008: Insertion of targets `other_bibtex` and `other_makeindex` in order to allow the calls of other `bibtex` and `makeindex` commands.

* 30 March 2008: Bug correction: scripts `slideint01` and `slideint02` updated to create links and removing them for the new logos.

* 07 March 2008: Removing of all the generation process of files `*.stoc` required by moodle. From now, this should to be entirely done in target `post-public`. Bug correction: Introduce a space between `pre-chapens` and figures in target `chapens` of file `02Course/Ressources/Makefile`.

* 18 February 2008:
    * Changes in `newslide`:
        * Removal of the style semcolor, to be replaced in slideint by the style color
        * Comment added to the last section explaining why there is no layers in PDF files, due to `dvipdfm`
    * Changes in `slideint`:
        * Bug correction: coloring is ok now for HTML pages, style color added with the option usenames so that all the dvips system colors can be used

* 25 January 2008:
```
Changes in packaging:
  * Replace dependency texlive-full by texlive + texlive-pstricks
  * Add dependency dviutils for dviselect
  * Rename /usr/share/slideint/examples into /usr/share/slideint/Examples
  * Add dependency to sun-java5-jdk
  * Replace dependency texlive by texlive-full (to include pstricks.sty)
  * Example directory in /usr/share/slideint/examples,
    /usr/share/doc/slideint/examples to be a link to the previous directory
Changes in slideint:
  * Correction bug link "suivant" missing in HTML pages:
    add a latex compilation when no bibbliography and no index
  * Correction bug target liens-ressources: in dependencies, this target must
    be called before the target pre-* because the execution of the latter
    may need results of the execution of the former
  * Null spacing between items with command \noitemspace: command called
    by default in slideint.sty
  * Command \license to be inserted by users in their presentations
  * Addition of tests in Makefile-figure before calling shell command convert
    so that output files are not overriden if they already exist
  * Factorisation of index style files
  * Bug correction : handling ISO character in shell makeindextex4ht.sh
  * Addition of command \slideindex to replace command \index
  * Remove rights x on regular files
  * Add directories Figures + examples of \includegraphics
  * Some additional latex style files in file configuration.tex
  * Correction bug: add redirect in 01Presentation/Makefile
```

* 29 December 2007: Correction of the debian package. Addition of a script to automate the updating and the deployment of the debian package. Creation of a corresponding RPM package.

* 20 December 2007: Complete refactoring of the directory structures. Complete refactoring of the makefiles. Creation of a debian package.

* 11 October 2007: New possibility to not introduce a "next" link in the HTML page of the title. This is done by the means of an optional argument of the commande `maketitle`: When no argument, the link is present; when an argument, the link is not present (it is then useable when all the content is/must be in one HTML page).

* 1 August 2006: New directory structure with the organisation of a dummy course with several chapters/presentations, a combine of these chapters in booklets, and the translation of the chapters in HTML pages.

* 16 July 2007: Correction bug in configuration of tex4ht. The configuration file `ConfigTex4ht/tex4ht.env` is copied from `/usr/share/texmf/tex4ht/tex4ht.env`.

* 16 July 2007: In directory `03TransparentsFrancaisTex4ht/ConfigTex4ht` modification of `GenerationMenu.java` so that toc entries that do correspond to a real file are removed from the menu. For information, this functionality is added for project `ap11bis`, for the translation of Labs exercices.

* 16 July 2007: Recoding of HTM pages generated by TeX4ht from `ISO-8859-15` to `HTML-4.0`.

* 12 January 2007: Correction of the bug that prevent the execution of the target `all` of the makefile of in the directory `03TransparentsFrancaisTex4ht`. Decomposition of the target `clean` into two targets, with the creation of the target `lightclean`.

* 10 November 2006: When combining several presentations in one document, 2 pages per page, except for title page + the page that follows, with the first page of every presentations on odd pages (in the final document): new command `\clearquadruplepage` and modification of the `Makefile` that makes the combination.

* 09 November 2006: `Makefile` modified to generate Postscript slides file with upside-down treatment.

* 06 November 2006: Addition the option `-file-line-error` to the `latex` commands in `Makefile`s.

* 04 November 2006: The new functionality is the possibility to write LaTeX command in slide titles. A second argument in command `*secslide*` in order to make the difference between titles in slides and titles in minitoc (through a `write` call). But be careful! The first argument must be treated in the `makefile` to remove LaTeX commands from the HTML menu (see the example in `ConfigTex4ht/Makefile`, line `sed ... \${TOC} > toc.toc)`.

* 24 October 2006: Correction of a major bug: A `\newpage` command is no more necessary everywhere in the user file for the generation of HTML pages with TeX4ht.

* 20 October 2006: Typography of footnotes using the `babel` command `\FrenchFootnotes`. Added to the file `configuration.tex`.

* 06 October 2006: Corrections to HTML fragments to pass successfully the [W3C Validator](http://validator.w3.org/) tests and add a link to the W3C Validator URL in generated HTML pages.

* 09 September 2006: Change `sed` scripts into the `makefile` of `ConfigTex4ht` of `03TransparentsFrancaisTex4ht` in order to only use HTML
accentued characters.

* 31 October 2005: Remove the extra space before the first word when using `\slidetextcolor`.

* 09 September 2005: All that is required for the generation of the toc for ganesha (this is very very specific to INT, sorry ;-) ).

* 03 August 2005: Correction of `\Configure{graphics*}`.

* 20 July 2005: Complete re-organisation of the source code + structuring into directories with complete, simple and self-explicit examples + one example using the combine class for gathering into a single document several presentations + translation into HTML pages of a presentation using TeX4ht (this part was previously the content of the `slideinttex4ht` picolibre project).


* 29 June 2005: Structuring of notes with the environments secnote, subsecnote, and subsubsecnote. secnote corresponds to section*... Possibility to have mini-tocs of subsecnotes and subsubsecnotes.

* 24 June 2005: By default, `\slidecolortrue`

* 17 June 2005: `\slidecolorfalse` and `\slidecolortrue` commands in order to disable or to enable coloring, by default no colors.

* 10 January 2005: presentation title inserted in the toc ; tests presentation with an index successful.

* 03 November 2004: `slideheight -2mm` to avoid overfull `vbox` with `parbox`, 2cm for the logo.

* 11 October 2004: Space between headers and body in article.

* 16 March 2004: New class `newslide.cls` and typos.

* 05 January 2004: Initial version
