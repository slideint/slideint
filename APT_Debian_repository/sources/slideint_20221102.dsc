Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20221102
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 10)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 5630d3013adb4cc8227b4e99625f38d1b768c54d 30327604 slideint_20221102.tar.gz
Checksums-Sha256:
 13ba3927f7cecedacef94ba8941a384ab8d9e7fa390acecae6b1ed3dffda43c7 30327604 slideint_20221102.tar.gz
Files:
 3e4e8d02126cd1fe7750001a19e924ec 30327604 slideint_20221102.tar.gz
