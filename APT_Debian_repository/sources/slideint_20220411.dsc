Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20220411
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 10)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 746242653df3780fa3893249af34acae7afe50cb 30326898 slideint_20220411.tar.gz
Checksums-Sha256:
 87052e5a561a4eacec264ea4609617cd3594a7f95201af4811bd1a80b2d5bd46 30326898 slideint_20220411.tar.gz
Files:
 1135c9d299c3cb894d6b7eb04d097b4f 30326898 slideint_20220411.tar.gz
