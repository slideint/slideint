Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20220428
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 10)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 672dcfac58fba5cd53c3c44a853bc5d3f52e1b02 30328700 slideint_20220428.tar.gz
Checksums-Sha256:
 12e6c9e6f9f4df669e857a203eb5a62816d4d19287b177ccfe471d756f92d633 30328700 slideint_20220428.tar.gz
Files:
 f188bb68f6e1063e87224dd82a590bf1 30328700 slideint_20220428.tar.gz
