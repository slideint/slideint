Format: 1.0
Source: slideint
Binary: slideint
Architecture: all
Version: 20220303
Maintainer: Denis Conan <denis.conan@telecom-sudparis.eu>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 10)
Package-List:
 slideint deb unknown extra arch=all
Checksums-Sha1:
 88876af09cc3f6a41f04755fe99c49a5c12d7e66 30354627 slideint_20220303.tar.gz
Checksums-Sha256:
 0c367abb6d5d3839875438bc0dec210127b41ff7127ba9f4981ea271df898c5f 30354627 slideint_20220303.tar.gz
Files:
 b129a19aead9d45a18780578cd0dfb61 30354627 slideint_20220303.tar.gz
